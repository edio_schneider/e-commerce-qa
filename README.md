# O que está sendo automatizado?
* É uma loja virtual para compras de vestuários.

# Qual a URL base que foi utilizada para o teste? 
* http://automationpractice.com/index.php

# Foi configurado o CI/CD no Gitlab? E como faço para ver o arquivo de Configurações? 
Sim. Na raiz do projeto tem um arquivo '.gitlab-ci.yml' que contem todas as configurações para execução.
Caso tenha duvida de como montar, pode verificar em: https://docs.gitlab.com/ee/ci/yaml/

# Como faço para verificar as ultimas execuções?
* Pode ir na opção no gitlab CI/CD > pipeline.

# Qual o status da ultima execução?
* Está aqui: [![pipeline status](https://gitlab.com/edio_schneider/e-commerce-qa/badges/master/pipeline.svg)](https://gitlab.com/edio_schneider/e-commerce-qa/-/commits/master)

# Como faço para ver os report gerados?
* Os reports da execução dos testes Reports estão no seguinte caminho  ...caminho_projeto_teste\e-commerce-qa\reports

# Quais as ferramentas que foram utilizadas para este projeto?
## São elas: 

* [Maven](https://maven.apache.org/) - Dependency Management
* [Intellij](https://www.jetbrains.com/idea/) - IDE
* [TestNG](https://testng.org/doc/) - Framework de teste
* [Selenium](https://www.seleniumhq.org/) - Selenium automates browsers



# Qual o comando que eu uso para executar todos os teste?
```
mvn clean test -D navegador=chrome
```

# Como faço para incluir um novo navegador para executar os teste?

* basta indicar no pom.xml o seguinte property:

```
  <properties>
                <db.navegador>Firefox</db.navegador>
                <db.url>http://automationpractice.com/index.php</db.url>
   </properties>
```

# Quais os padrões de projeto que foram utilizadas neste projeto?

* [Page Object](http://www.codeatest.com/page-object-testes-aceitacao-organizados/)
* [Fluent Page Object](https://www.automatetheplanet.com/fluent-page-object-pattern/) 
* [Factory](https://www.baeldung.com/java-abstract-factory-pattern)


