package com.br;

import com.br.dataproviders.DataProviderProdutosCompra;
import com.br.util.BaseTest;
import org.testng.annotations.Test;

public class ShoppingCartPaymentTest extends BaseTest {
    private static String MENSAGEM_BANK_WIRE_PAYMENT = "BANK-WIRE PAYMENT";
    private static String MENSAGEM_FOLLOING_CURRENT = "We allow the following currency to be sent via bank wire";
    private static String MENSAGEM_ACCOUNT_CURRENTY = "Bank wire account information will be displayed on the next page";
    private static String MENSAGEM_BANK_WIRE_ORDER_PAYMENT_COMPLETE = "Your order on My Store is complete.";
    private static String MENSAGEM_PLEASE_CHOOSE_YOUR_PAYMENT_METHOD = "PLEASE CHOOSE YOUR PAYMENT METHOD";


    @Test(dataProvider = "produto", dataProviderClass = DataProviderProdutosCompra.class)
    public void validarBankWireOrderPaymentCompleteMensagens(String nomeProduto) {
        homePage.entrar()
                .entrarTelaLogin()
                .realizarLogin(usuario)
                .adicionarItemNoCarrinho(nomeProduto)
                .irParaAbaPayment()
                .escolherOpcaoPaybyBankWire()
                .finalizarPedido()
                .validarMensagemTelaOrderPaymentComplete(MENSAGEM_BANK_WIRE_ORDER_PAYMENT_COMPLETE);
    }

    @Test(dataProvider = "produto", dataProviderClass = DataProviderProdutosCompra.class)
    public void validarBankWirePaymentMensagens(String nomeProduto) {
        homePage.entrar()
                .entrarTelaLogin()
                .realizarLogin(usuario)
                .adicionarItemNoCarrinho(nomeProduto)
                .irParaAbaPayment()
                .validarmeMensagemTituloTelaPayment(MENSAGEM_PLEASE_CHOOSE_YOUR_PAYMENT_METHOD)
                .escolherOpcaoPaybyBankWire()
                .validarMensagemTelaPayment(MENSAGEM_BANK_WIRE_PAYMENT)
                .validarMensagemTelaPayment(MENSAGEM_ACCOUNT_CURRENTY)
                .validarMensagemTelaPayment(MENSAGEM_FOLLOING_CURRENT);
    }

    @Test(dataProvider = "produto", dataProviderClass = DataProviderProdutosCompra.class)
    public void validarSeProdutoNoCarrinhoIgualNaAbaPayment(String nomeProduto) {
        homePage.entrar()
                .entrarTelaLogin()
                .realizarLogin(usuario)
                .adicionarItemNoCarrinho(nomeProduto)
                .irParaAbaPayment()
                .validaProdutoIgualCarrinho(nomeProduto);
    }
}
