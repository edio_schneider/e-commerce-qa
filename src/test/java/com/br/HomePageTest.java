package com.br;

import com.br.util.BaseTest;
import org.testng.annotations.Test;


public class HomePageTest extends BaseTest {

    @Test
    public void validarCamposDaTelaHome() {
        homePage.validarSeEstouNaTelaHome()
                .validarCampoBotaoLogin();
    }
}
