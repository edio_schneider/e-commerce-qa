package com.br;

import com.br.dataproviders.DataProviderProdutosCompra;
import com.br.util.BaseTest;
import org.testng.annotations.Test;

public class ShoppingTest extends BaseTest {

    @Test(dataProvider = "produtos", dataProviderClass = DataProviderProdutosCompra.class)
    public void validarCompraComUmProdutoPorPedido(String nomeProduto) {
        homePage.entrar()
                .entrarTelaLogin()
                .realizarLogin(usuario)
                .adicionarItemNoCarrinho(nomeProduto)
                .irParaEscolherTipoPagamento()
                .escolherOpcaoPaybyBankWire()
                .finalizarPedido()
                .realizarLogout();
    }

    @Test(dataProvider = "produto", dataProviderClass = DataProviderProdutosCompra.class)
    public void validaFluxoPagamentoBank(String nomeProduto) {
        homePage.entrar()
                .entrarTelaLogin()
                .realizarLogin(usuario)
                .adicionarItemNoCarrinho(nomeProduto)
                .irParaEscolherTipoPagamento()
                .escolherOpcaoPaybyBankWire()
                .finalizarPedido()
                .realizarLogout();
    }


    @Test(dataProvider = "produto", dataProviderClass = DataProviderProdutosCompra.class)
    public void validaFluxoPagamentoCheck(String nomeProduto) {
        homePage.entrar()
                .entrarTelaLogin()
                .realizarLogin(usuario)
                .adicionarItemNoCarrinho(nomeProduto)
                .irParaEscolherTipoPagamento()
                .escolherOpcaoPaybyCheck()
                .finalizarPedido()
                .realizarLogout();
    }

    @Test(dataProvider = "produto", dataProviderClass = DataProviderProdutosCompra.class)
    public void validarVariosProdutosPorPedido(String nomeProduto) {
        homePage.entrar()
                .entrarTelaLogin()
                .realizarLogin(usuario)
                .adicionarItemNoCarrinho(nomeProduto)
                .pegarQuantidadeItemCarrinhoAnterior()
                .adicionarItemNoCarrinho(nomeProduto)
                .validarSeUmNovoItemFoiAdicionado()
                .irParaEscolherTipoPagamento()
                .escolherOpcaoPaybyBankWire()
                .finalizarPedido()
                .realizarLogout();
    }

    @Test(dataProvider = "inserirDoisProdutosAoMesmoTempo", dataProviderClass = DataProviderProdutosCompra.class)
    public void validarAdicionarDoisItemERemover(String nomeProduto1, String nomeProduto2) {
        homePage.entrar()
                .entrarTelaLogin()
                .realizarLogin(usuario)
                .adicionarItemNoCarrinho(nomeProduto1)
                .pegarQuantidadeItemCarrinhoAnterior()
                .adicionarItemNoCarrinho(nomeProduto2)
                .removerItemDoCarrinho()
                .irParaEscolherTipoPagamento()
                .validarSeBotaoRemoveu()
                .escolherOpcaoPaybyBankWire()
                .finalizarPedido()
                .realizarLogout();
    }

    @Test(dataProvider = "produto", dataProviderClass = DataProviderProdutosCompra.class)
    public void validarSeOProdutoAdicionadoNoCarrinhoEIgualSelecionado(String nomeProduto) {
        homePage.entrar()
                .entrarTelaLogin()
                .realizarLogin(usuario)
                .adicionarItemNoCarrinho(nomeProduto)
                .irParaEscolherTipoPagamento()
                .validaProdutoCarrinhoAdicionadoIgualAoSelecionado(nomeProduto)
                .escolherOpcaoPaybyBankWire()
                .finalizarPedido()
                .realizarLogout();
    }
}
