package com.br;

import com.br.dataproviders.DataProviderProdutosCompra;
import com.br.util.BaseTest;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;

public class ShoppingCartSummaryTest extends BaseTest {
    private static final Logger logger = Logger.getLogger(ShoppingTest.class);

    @Test(dataProvider = "produto", dataProviderClass = DataProviderProdutosCompra.class)
    public void validarNomeProdutoSelecionadoSummary(String nomeProduto) {
        homePage.entrar()
                .entrarTelaLogin()
                .realizarLogin(usuario)
                .adicionarItemNoCarrinho(nomeProduto)
                .irParaShoppingCartSummary()
                .validarSeProdutoSelecionadoExisteNoSummary(nomeProduto);
    }
}
