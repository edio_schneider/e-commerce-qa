package com.br.validacao;

import org.apache.log4j.Logger;
import org.testng.Assert;

import java.util.Arrays;

public class ValidacaoShoppingCartPaymentPage {
    private static final Logger logger = Logger.getLogger(ValidacaoShoppingCartPaymentPage.class);


    public void validarListaDeMensagem(String mensagemDaTela, String mensagem) {
        logger.info("validando se existe a mensagem na tela");
        boolean achouMensagem = false;
        for (String mensagemlist : Arrays.asList(mensagemDaTela.split("\\r?\\n"))) {
            if (mensagemlist.contains(mensagem)) {
                achouMensagem = true;
            }
        }
        Assert.assertTrue(achouMensagem, "Não existe a mensagem na aba Payment: " + mensagem);
    }

    public void validarMensagem(String mensagemDaTela, String mensagem) {
        logger.info("validando se existe a mensagem na tela");
        Assert.assertEquals(mensagemDaTela, mensagem);
    }

    public void validarTituloDaTelaPayment(String mensagemDaTela, String mensagem) {
        logger.info("validando se existe o titulo da tela");
        Assert.assertEquals(mensagemDaTela, mensagem);
    }

    public void validarProdutoAbaPaymentIgualCarrinho(String nomeProdutoCarrinho, String nomeProdutoAbaPayment, String valorProdutoCarrinho, String valorProdutoAbaPayment) {
        logger.info("Validando se o produto que esta na aba payment é igual ao que foi selecionado no carrinho");
        Assert.assertEquals(nomeProdutoCarrinho, nomeProdutoAbaPayment, "O nome do Produto deveria ser igual ao que esta no carrinho");
        Assert.assertEquals(valorProdutoCarrinho, valorProdutoAbaPayment, "O valor do Produto deveria ser igual ao que esta no carrinho");

    }
}
