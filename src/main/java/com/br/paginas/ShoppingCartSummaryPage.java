package com.br.paginas;

import com.br.paginas.elementosdapagina.ElementoShoppingCartSummaryPage;
import com.br.util.Reports;
import com.br.validacao.ValidacaoShoppingCartSummaryPage;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class ShoppingCartSummaryPage extends BasePage {

    private static final Logger logger = Logger.getLogger(ShoppingCartSummaryPage.class);
    private ElementoShoppingCartSummaryPage elementoShoppingCartSummary;
    private ValidacaoShoppingCartSummaryPage validacaoShoppingCartSummaryPage;
    private static String NOME_DO_ARQUIVO_GERADO_REPORT = "report_tela_shopping_summary_page";
    private static String TITULO_REPORT = "Validação da Tela Shopping Summary Page";

    public ShoppingCartSummaryPage(WebDriver driver, Reports reports) {
        super(driver, reports);
        logger.info("iniciando o construtor ShoppingCartSummaryPage");
        elementoShoppingCartSummary = new ElementoShoppingCartSummaryPage(driver, reports);
        validacaoShoppingCartSummaryPage = new ValidacaoShoppingCartSummaryPage();
        reports.criarReport(NOME_DO_ARQUIVO_GERADO_REPORT, TITULO_REPORT);
    }

    public ShoppingCartSummaryPage validarSeProdutoSelecionadoExisteNoSummary(String nomeProduto) {
        logger.info("iniciando a validadacao  validarSeProdutoSelecionadoExisteNoSummary");
        validacaoShoppingCartSummaryPage.validarSeoItemSelecionadoExisteNoSummary(elementoShoppingCartSummary.getNomeDoProdutoSelecionadoSumary(), nomeProduto);
        reports.logAssertPassou("Validacao - O produto seleciona exite na aba Summary");
        logger.info("finalizado a validadacao  validarSeProdutoSelecionadoExisteNoSummary");
        return this;
    }
}
