package com.br.paginas.elementosdapagina;

import com.br.paginas.BasePage;
import com.br.util.Reports;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ElementoHomePage extends BasePage {

    @FindBy(xpath = "//a[contains(@title, 'View my shopping cart')]")
    private WebElement campoListaDeProdutosCarrinho;

    @FindBy(id = "index")
    private WebElement logoDaTelaPrincipal;

    @FindBy(className = "login")
    protected WebElement botaoLogin;

    @FindBy(name = "search_query")
    protected WebElement campoDePesquisa;

    public ElementoHomePage(WebDriver driver, Reports reports) {
        super(driver, reports);
    }

    public WebElement getCampoListaDeProdutosCarrinho() {
        return campoListaDeProdutosCarrinho;
    }

    public WebElement getLogoDaTelaPrincipal() {
        return logoDaTelaPrincipal;
    }

    public WebElement getBotaoLogin() {
        return botaoLogin;
    }

    public WebElement getCampoDePesquisa() {
        return campoDePesquisa;
    }

}
