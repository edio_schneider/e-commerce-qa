package com.br.paginas.elementosdapagina;

import com.br.paginas.BasePage;
import com.br.util.Reports;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ElementoShoppingCartSummaryPage extends BasePage {

    @FindBy(className = "product-name")
    private List<WebElement> nomeDoProdutoSelecionadoSumary;

    public ElementoShoppingCartSummaryPage(WebDriver driver, Reports reports) {
        super(driver, reports);
    }

    public List<WebElement> getNomeDoProdutoSelecionadoSumary() {
        return nomeDoProdutoSelecionadoSumary;
    }

}
