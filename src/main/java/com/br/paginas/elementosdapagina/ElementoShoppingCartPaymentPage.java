package com.br.paginas.elementosdapagina;

import com.br.paginas.BasePage;
import com.br.util.Reports;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ElementoShoppingCartPaymentPage extends BasePage {


    @FindBy(id = "total_price")
    private WebElement campoValorTotalProdutoAbaPayment;

    @FindBy(className = "cheque-box")
    private WebElement campoMensagemBankWirePayment;

    @FindBy(className = "cheque-indent")
    private WebElement campoMensagemBankWireOrderPaymentComplete;

    @FindBy(className = "page-heading")
    private WebElement campoTituloPleaseChooseYourPaymentMethod;

    @FindBy(className = "cart_block_product_name")
    private WebElement campoNomeProdutoNoCarrinhoSelecionado;

    @FindBy(className = "ajax_block_cart_total")
    private WebElement campoValorProdutoNoCarrinhoSelecionado;

    @FindBy(xpath = "//a[contains(@title, 'View my shopping cart')]")
    private WebElement campoListaDeProdutosCarrinho;

    @FindBy(xpath = "//a[contains(@class,'bankwire')]")
    private WebElement botaoPaybyBankWirePayment;

    @FindBy(xpath = "//a[contains(@class,'cheque')]")
    private WebElement botaoPaybyCheck;

    @FindBy(xpath = "//button[contains(@class,'button-medium')]")
    private WebElement botaoIConfirmMyOrderPayment;


    public WebElement getImagemProdutoAbaPayment(String nomeProduto) {
        WebElement e = driver.findElement(By.xpath("//img[contains(@alt,'" + nomeProduto + "')]"));
        return e;
    }

    private WebElement imagemProdutoAbaPayment;

    public WebElement getCampoValorProdutoNoCarrinhoSelecionado() {
        return new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(campoValorProdutoNoCarrinhoSelecionado));
        //campoValorProdutoNoCarrinhoSelecionado;
    }

    public WebElement getCampoValorTotalProdutoAbaPayment() {
        return campoValorTotalProdutoAbaPayment;
    }

    public WebElement getCampoNomeProdutoNoCarrinhoSelecionado() {
        return campoNomeProdutoNoCarrinhoSelecionado;
    }

    public WebElement getBotaoIConfirmMyOrderPayment() {
        return botaoIConfirmMyOrderPayment;
    }

    public ElementoShoppingCartPaymentPage(WebDriver driver, Reports reports) {
        super(driver, reports);
    }

    public WebElement getBotaoPaybyBankWirePayment() {
        return botaoPaybyBankWirePayment;
    }

    public WebElement getBotaoPaybyCheck() {
        return botaoPaybyCheck;
    }

    public WebElement getCampoMensagemBankWirePayment() {
        return campoMensagemBankWirePayment;
    }

    public WebElement getCampoMensagemBankWireOrderPaymentComplete() {
        return campoMensagemBankWireOrderPaymentComplete;
    }

    public WebElement getCampoTituloPleaseChooseYourPaymentMethod() {
        return campoTituloPleaseChooseYourPaymentMethod;
    }

    public WebElement getCampoListaDeProdutosCarrinho() {
        Actions builder = new Actions(driver);
        builder.moveToElement(campoListaDeProdutosCarrinho).perform();
        return campoListaDeProdutosCarrinho;
    }
}
