package com.br.paginas.elementosdapagina;

import com.br.paginas.BasePage;
import com.br.util.Reports;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ElementoShopppingPage extends BasePage {

    @FindBy(id = "search_query_top")
    private WebElement campoDePesquisa;

    @FindBy(id = "add_to_cart")
    private WebElement botaoAddToCart;

    @FindBy(id = "cgv")
    private WebElement botaoCheckBoxShipping;

    @FindBy(name = "processAddress")
    private WebElement botaoProceedToCheckoutAddress;

    @FindBy(name = "processCarrier")
    private WebElement botaoProceedToCheckoutShipping;

    @FindBy(xpath = "//a[contains(@title, 'View my shopping cart')]")
    private WebElement campoListaDeProdutosCarrinho;

    @FindBy(xpath = "//img[contains(@itemprop, 'image')]")
    private WebElement imagemDoProdutoClicavel;

    @FindBy(xpath = "//a[contains(@title, 'Proceed to checkout')]")
    private WebElement botaoProceedToCheckout;

    @FindBy(xpath = "//a[contains(@class, 'logout')]")
    private WebElement botaologout;

    @FindBy(xpath = "//a[contains(@class,'standard-checkout')]")
    private WebElement botaoProceedToCheckoutSummary;

    @FindBy(xpath = "//a[contains(@class,'bankwire')]")
    private WebElement botaoPaybyBankWirePayment;

    @FindBy(xpath = "//a[contains(@class,'cheque')]")
    private WebElement botaoPaybyCheck;

    @FindBy(xpath = "//button[contains(@class,'button-medium')]")
    private WebElement botaoIConfirmMyOrderPayment;

    @FindBy(xpath = "//span[contains(@class,'ajax_cart_quantity')]")
    private WebElement quantidadeItemCarrinho;

    @FindBy(xpath = "//a[contains(@title,'remove this product from my cart')]")
    private List<WebElement> botaoRemoverItemCarrinho;


    public ElementoShopppingPage(WebDriver driver, Reports reports) {
        super(driver, reports);
    }

    public WebElement getCampoDePesquisa() {
        return campoDePesquisa;
    }


    public WebElement getQuantidadeItemCarrinho() {
        return quantidadeItemCarrinho;
    }

    public List<WebElement> getBotaoRemoverItemCarrinho() {

        return botaoRemoverItemCarrinho;
    }

    public void setBotaoRemoverItemCarrinho(List<WebElement> botaoRemoverItemCarrinho) {
        this.botaoRemoverItemCarrinho = botaoRemoverItemCarrinho;
    }

    public WebElement getBotaoAddToCart() {
        return botaoAddToCart;
    }

    public WebElement getBotaoCheckBoxShipping() {
        return botaoCheckBoxShipping;
    }


    public WebElement getBotaoPaybyCheck() {
        return botaoPaybyCheck;
    }


    public WebElement getBotaoProceedToCheckoutAddress() {
        return botaoProceedToCheckoutAddress;
    }

    public WebElement getBotaoProceedToCheckoutShipping() {
        return botaoProceedToCheckoutShipping;
    }

    public WebElement getImagemDoProdutoClicavel() {
        return imagemDoProdutoClicavel;
    }

    public WebElement getBotaoProceedToCheckout() {
        return botaoProceedToCheckout;
    }

    public WebElement getBotaologout() {
        return botaologout;
    }

    public WebElement getBotaoProceedToCheckoutSummary() {
        return botaoProceedToCheckoutSummary;
    }

    public WebElement getBotaoPaybyBankWirePayment() {
        return botaoPaybyBankWirePayment;
    }

    public WebElement getBotaoIConfirmMyOrderPayment() {
        return botaoIConfirmMyOrderPayment;
    }

    public String tamanhoListaCarrinho() {
        return quantidadeItemCarrinho.getText();
    }

    public WebElement getCampoListaDeProdutosCarrinho() {
        Actions builder = new Actions(driver);
        builder.moveToElement(campoListaDeProdutosCarrinho).perform();
        return campoListaDeProdutosCarrinho;
    }

    public boolean existeProdutoPorNomeNoCarrinho(String nomeDoProduto) {
        WebElement produtoCarrinho = driver.findElement(By.xpath("//a[contains(@title,'" + nomeDoProduto + "')]"));
        return produtoCarrinho.isEnabled() && produtoCarrinho.isDisplayed() ? true : false;
    }
}
