package com.br.paginas;

import com.br.paginas.elementosdapagina.ElementoSignInPage;
import com.br.util.Reports;
import com.br.validacao.ValidacaoSignInPage;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class SignInPage extends BasePage {

    private static final Logger logger = Logger.getLogger(SignInPage.class);
    private ValidacaoSignInPage validacaoSignInPage;
    private ElementoSignInPage elementoSignInPage;

    public SignInPage(WebDriver driver, Reports reports) {
        super(driver, reports);
        validacaoSignInPage = new ValidacaoSignInPage();
        elementoSignInPage = new ElementoSignInPage(driver, reports);
    }

    public LoginPage entrarTelaLogin() {
        logger.info("entrando para logar na tela");
        return new LoginPage(driver, reports);
    }

    public void validacaoTelaLogin() {
        validacaoSignInPage.validarCamposTelaSignIn(elementoSignInPage);
    }
}
