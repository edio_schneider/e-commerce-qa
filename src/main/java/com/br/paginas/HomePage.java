package com.br.paginas;

import com.br.paginas.elementosdapagina.ElementoHomePage;
import com.br.util.Reports;
import com.br.validacao.ValidationHomePage;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class HomePage extends BasePage {

    private static final Logger logger = Logger.getLogger(HomePage.class);
    private ValidationHomePage validationHomePage;
    private ElementoHomePage elementoHomePage;
    private static String NOME_DO_ARQUIVO_GERADO_REPORT = "report_tela_home";
    private static String TITULO_REPORT = "Validação da Tela Home";

    public HomePage(WebDriver driver, Reports reports) {
        super(driver, reports);
        validationHomePage = new ValidationHomePage();
        elementoHomePage = new ElementoHomePage(driver, reports);
        reports.criarReport(NOME_DO_ARQUIVO_GERADO_REPORT, TITULO_REPORT);
    }

    public SignInPage entrar() {
        logger.info("clicando no botão SigIn");
        elementoHomePage.getBotaoLogin().click();
        reports.logInfo("clicando no botão SigIn");
        return new SignInPage(driver, reports);
    }

    public HomePage validarCampoBotaoLogin() {
        validationHomePage.validacaoBotaoLogin(elementoHomePage.getBotaoLogin());
        reports.logAssertPassou("Passou na validação botão login");
        return this;
    }

    public HomePage validarSeEstouNaTelaHome() {
        validationHomePage.validacaoSeEstouNaTelaHome(elementoHomePage);
        reports.logAssertPassou("Passou na validação estou na tela Home");
        return this;
    }
}
