package com.br.paginas;

import com.br.paginas.elementosdapagina.ElementoShoppingCartPaymentPage;
import com.br.util.Reports;
import com.br.validacao.ValidacaoShoppingCartPaymentPage;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class ShoppingCartPaymentPage extends BasePage {

    private static final Logger logger = Logger.getLogger(ShoppingCartPaymentPage.class);
    private ElementoShoppingCartPaymentPage elementoShoppingCartPaymentPage;
    private ValidacaoShoppingCartPaymentPage validacaoShoppingCartPaymentPage;
    private static String NOME_DO_ARQUIVO_GERADO_REPORT = "report_tela_shopping_payment_page";
    private static String TITULO_REPORT = "Validação da Tela Shopping Payment Page";

    public ShoppingCartPaymentPage(WebDriver driver, Reports reports) {
        super(driver, reports);
        logger.info("iniciando o construtor ShoppingCartPaymentPage");
        elementoShoppingCartPaymentPage = new ElementoShoppingCartPaymentPage(driver, reports);
        validacaoShoppingCartPaymentPage = new ValidacaoShoppingCartPaymentPage();
    }

    public ShoppingCartPaymentPage validarMensagemTelaPayment(String mensagem) {
        logger.info("iniciando a validadacao  validarMensagemdaTelaPayment");
        validacaoShoppingCartPaymentPage.validarListaDeMensagem(elementoShoppingCartPaymentPage.getCampoMensagemBankWirePayment().getText()
                , mensagem);
        reports.logAssertPassou("Validacao - Existe as mensagem na tela Payment");
        logger.info("finalizado a validadacao  validarMensagemdaTelaPayment");
        return this;
    }

    public ShoppingCartPaymentPage validarmeMensagemTituloTelaPayment(String mensagem) {
        logger.info("iniciando a validadacao  validarmeMensagemTituloTelaPayment");
        validacaoShoppingCartPaymentPage.validarTituloDaTelaPayment(elementoShoppingCartPaymentPage.getCampoTituloPleaseChooseYourPaymentMethod().getText()
                , mensagem);
        reports.logAssertPassou("Validacao - Existe o titulo na tela Payment");
        logger.info("finalizado a validadacao  validarmeMensagemTituloTelaPayment");
        return this;
    }

    public ShoppingCartPaymentPage validarMensagemTelaOrderPaymentComplete(String mensagem) {
        logger.info("iniciando a validadacao  validarMensagemdaTelaPayment");
        validacaoShoppingCartPaymentPage.validarMensagem(elementoShoppingCartPaymentPage.getCampoMensagemBankWireOrderPaymentComplete().getText()
                , mensagem);
        logger.info("finalizado a validadacao  validarMensagemdaTelaPayment");
        reports.logAssertPassou("Validacao - Existe as mensagem na aba Order Payment");
        return this;
    }

    public ShoppingCartPaymentPage finalizarPedido() {
        logger.info("Confirmando a Ordem");
        elementoShoppingCartPaymentPage.getBotaoIConfirmMyOrderPayment().click();
        logger.info("Confirmado a Ordem");
        return this;
    }

    public ShoppingCartPaymentPage validaProdutoIgualCarrinho(String nomeProduto) {
        elementoShoppingCartPaymentPage.getCampoListaDeProdutosCarrinho();
        logger.info("Pegando as informações do carrinho");
        String nomeProdutoCarrinho = elementoShoppingCartPaymentPage.getCampoNomeProdutoNoCarrinhoSelecionado().getAttribute("title");
        String valorProdutoCarrinho = elementoShoppingCartPaymentPage.getCampoValorProdutoNoCarrinhoSelecionado().getText();
        logger.info("Pegando as informações dda aba Payment");
        String nomeProdutoAbaPayment = elementoShoppingCartPaymentPage.getImagemProdutoAbaPayment(nomeProduto).getAttribute("alt");
        String valorProdutoAbaPayment = elementoShoppingCartPaymentPage.getCampoValorTotalProdutoAbaPayment().getText();
        validacaoShoppingCartPaymentPage.validarProdutoAbaPaymentIgualCarrinho(nomeProdutoCarrinho,nomeProdutoAbaPayment, valorProdutoCarrinho,valorProdutoAbaPayment);
        reports.logAssertPassou("Validacao - O Produdo que esta na aba Payment e no carrinho são iguais");
        return this;
    }

    public ShoppingCartPaymentPage escolherOpcaoPaybyBankWire() {
        elementoShoppingCartPaymentPage.getBotaoPaybyBankWirePayment().click();
        logger.info("Escolhendo a opção Pay by Bank Wire");
        return this;
    }

    public ShoppingCartPaymentPage escolherOpcaoPaybyCheck() {
        elementoShoppingCartPaymentPage.getBotaoPaybyCheck().click();
        logger.info("Escolhendo a opção Pay by Check");
        return this;
    }
}
