package com.br.paginas;

import com.br.util.Reports;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public abstract class BasePage {

    protected WebDriver driver;
    protected Reports reports;


    public BasePage(WebDriver driver, Reports reports) {
        this.driver = driver;
        this.reports = reports;
        PageFactory.initElements(this.driver, this);
    }

    public void gerarReport() {
        reports.gerarReport();
    }

}
