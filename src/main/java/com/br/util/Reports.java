package com.br.util;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Reports {

    private ExtentReports extent;
    private ExtentTest extentTest;


    public ExtentTest getExtentTest() {
        return extentTest;
    }

    public void setExtentTest(ExtentTest extentTest) {
        this.extentTest = extentTest;
    }

    public ExtentReports getExtent() {
        return extent;
    }

    public void setExtent(ExtentReports extent) {
        this.extent = extent;
    }

    public void criarReport(String nomeArquivoReport, String tituloReport) {
        String extentReportFile = System.getProperty("user.dir")
                + "\\reports\\" + nomeArquivoReport + ".html";
        String extentReportImage = System.getProperty("user.dir")
                + "\\reports\\" + nomeArquivoReport + ".png";
        extent = new ExtentReports(extentReportFile, false);
        extentTest = extent.startTest(tituloReport);

    }

    public void logInfo(String mensagem) {
        extentTest.log(LogStatus.INFO, mensagem);

    }

    public void logAssertPassou(String mensagemValidacaoPassou) {
        extentTest.log(LogStatus.PASS, mensagemValidacaoPassou);
    }

    public void gerarReport() {
        extent.endTest(extentTest);
        extent.flush();
    }

}
