package com.br.configuracao;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.util.concurrent.TimeUnit;

public class Drivers {
    public static WebDriver getFirefoxDriver() {
        WebDriverManager.firefoxdriver().setup();
        return configBrowser(new FirefoxDriver());
    }

    public static WebDriver getSafariDriver() {
        return configBrowser(new SafariDriver());
    }

    public static WebDriver getInternetExplorerDriver() {
        WebDriverManager.iedriver().setup();
        return configBrowser(new InternetExplorerDriver());
    }

    public static WebDriver getOperaDriver() {
        WebDriverManager.operadriver().setup();
        return configBrowser(new OperaDriver());
    }

    public static WebDriver getChromeDriver() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = configBrowserChrome();
        return configBrowser(driver);
    }

    private static ChromeDriver configBrowserChrome() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("enable-automation");
        return new ChromeDriver(options);
    }

    private static WebDriver configBrowser(WebDriver driver) {
        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        return driver;
    }
}
